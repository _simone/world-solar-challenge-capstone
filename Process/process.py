import pandas as pd
import os

directory = 'D:/BoM'


def process(filename):
    df = pd.read_csv('D:/BoM/' + filename, delimiter=',', low_memory=False)

    # Remove everything outside of October
    october = df[df.MM == 10]

    # Removes spaces in column names and makes all lowercase
    october.columns = df.columns.str.strip().str.lower().str.replace(' ', '_').str.replace('(', '').str.replace(')', '')

    # removes unneeded data
    october = october.loc[:, 'hm':'wind_direction_in_degrees_true']
    october = october[october.year_month_day_hour_minutes_in_yyyy > 1999]
    # october = october[october.hh24 > 8]
    # october = october[october.hh24 < 19]

    october = october.drop(
        ['hm', 'precipitation_in_last_10_minutes_in_mm', 'quality_of_precipitation_in_last_10_minutes',
         'quality_of_precipitation_since_9am_local_time', 'quality_of_air_temperature',
         'wet_bulb_temperature_in_degrees_c', 'quality_of_wet_bulb_temperature',
         'quality_of_dew_point_temperature', 'quality_of_relative_humidity',
         'year_month_day_hour_minutes_in_yyyy.1', 'mm.1', 'dd.1', 'hh24.1',
         'mi_format_in_local_standard_time'], axis=1)

    october.rename(columns={'year_month_day_hour_minutes_in_yyyy': 'year'}, inplace=True)
    october.rename(columns={'mm': 'month'}, inplace=True)
    october.rename(columns={'dd': 'day'}, inplace=True)
    october.rename(columns={'hh24': 'hour'}, inplace=True)
    october.rename(columns={'mi_format_in_local_time': 'minute'}, inplace=True)
    october.rename(columns={'air_temperature_in_degrees_c': 'air_temp'}, inplace=True)
    october.rename(columns={'dew_point_temperature_in_degrees_c': 'dew_point'}, inplace=True)
    october.rename(columns={'precipitation_since_9am_local_time_in_mm': 'precipitation'}, inplace=True)
    october.rename(columns={'relative_humidity_in_percentage_%': 'humidity'}, inplace=True)
    october.rename(columns={'wind_speed_in_km/h': 'wind_speed'}, inplace=True)
    october.rename(columns={'wind_direction_in_degrees_true': 'wind_direction'}, inplace=True)

    october.to_csv('D:/BoM/Processed/' + filename)

for filename in os.listdir(directory):
    if filename.endswith(".txt"):
        process(filename)
    else:
        continue
