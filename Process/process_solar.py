import pandas as pd
import os

directory = 'D:/BoM/Solar'


def process(filename):
    df = pd.read_csv('D:/BoM/Solar/' + filename, delimiter=',', low_memory=False)

    # Remove everything outside of October
    october = df

    # Removes spaces in column names and makes all lowercase
    october.columns = df.columns.str.strip().str.lower().str.replace(' ', '_').str.replace('(', '').str.replace(')', '')

    october.rename(columns={'day_month_year_in_dd/mm/yyyy_format': 'date'}, inplace=True)

    # converts column to datetime format
    october['date'] = pd.to_datetime(october['date'])

    october = october.loc[(october['date'].dt.month == 10) & (october['date'].dt.year > 1999)]

    october['date'] = october['date'].dt.date

    october.to_csv('D:/BoM/Processed/Solar/' + filename)


for filename in os.listdir(directory):
    if filename.endswith(".txt"):
        process(filename)
    else:
        continue
