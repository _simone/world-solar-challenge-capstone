import pandas as pd
import matplotlib.pyplot as plt

# Code I created for my capstone projcet using skills learnt from unit SIT384

pd.options.mode.chained_assignment = None  # disable error message

df = pd.read_csv('C:/Users/Josh/Documents/Uni/ProjectDesign/Processed/Josh(10).csv', delimiter=',')


def temperature(date):
    year = df[df.year_month_day_hour_minutes_in_yyyy == date]

    year.air_temperature_in_degrees_c = year.air_temperature_in_degrees_c.astype('float64')

    max = year.groupby(['dd'], sort=False)['air_temperature_in_degrees_c'].max()
    min = year.groupby(['dd'], sort=False)['air_temperature_in_degrees_c'].min()
    mean = year.groupby(['dd'], sort=False)['air_temperature_in_degrees_c'].mean()

    plt.figure(figsize=(15, 5), dpi=100)
    max.plot(kind='line', c='red', label='Max Temp')
    mean.plot(kind='line', c='green', label='Mean Temp')
    min.plot(kind='line', c='blue', label='Min Temp')
    plt.xlabel('Day')
    plt.ylabel('Temperature in C\N{DEGREE SIGN}')
    plt.title('Temperatures in October ' + str(date) + '\n Station: ' + df.station_number[1].astype('str'))
    plt.xticks(range(1, 32))
    plt.grid(linestyle=':')
    plt.legend()
    # Remove comment below to save figs
    '''plt.savefig(
        'C:/Users/Josh/Documents/Uni/ProjectDesign/Processed/Test/' + str(date) + '_' + df.station_number[1].astype(
            'str') + '.png')'''

    plt.show()


# Gets Max temps for each year per day and plots them
plt.figure(figsize=(15, 5), dpi=100)
for x in range(2014, 2019):
    yes = df[df.year_month_day_hour_minutes_in_yyyy == x]
    yes.air_temperature_in_degrees_c = yes.air_temperature_in_degrees_c.astype('float64')
    max = yes.groupby(['dd'], sort=False)['air_temperature_in_degrees_c'].max()
    max.plot(kind='line', label=x)
plt.xlabel('Day')
plt.ylabel('Temperature in C\N{DEGREE SIGN}')
plt.title('Max Temperatures in October Per Year\n Station: ' + df.station_number[1].astype('str'))
plt.xticks(range(1, 32))
plt.grid(linestyle=':')
plt.legend()
# remove below comment to save figs
'''plt.savefig(
    'C:/Users/Josh/Documents/Uni/ProjectDesign/Processed/Test/max_' + df.station_number[1].astype('str') + '.png')'''
plt.show()

# Gets mean temps for each year per day and plots them
plt.figure(figsize=(15, 5), dpi=100)
for x in range(2014, 2019):
    yes = df[df.year_month_day_hour_minutes_in_yyyy == x]
    yes.air_temperature_in_degrees_c = yes.air_temperature_in_degrees_c.astype('float64')
    mean = yes.groupby(['dd'], sort=False)['air_temperature_in_degrees_c'].mean()
    mean.plot(kind='line', label=x)
plt.xlabel('Day')
plt.ylabel('Temperature in C\N{DEGREE SIGN}')
plt.title('Average Temperatures in October Per Year\n Station: ' + df.station_number[1].astype('str'))
plt.xticks(range(1, 32))
plt.grid(linestyle=':')
plt.legend()
# remove below comment to save figs
'''plt.savefig(
    'C:/Users/Josh/Documents/Uni/ProjectDesign/Processed/Test/mean_' + df.station_number[1].astype('str') + '.png')'''
plt.show()

# Gets min temps for each year per day and plots them
plt.figure(figsize=(15, 5), dpi=100)
for x in range(2014, 2019):
    yes = df[df.year_month_day_hour_minutes_in_yyyy == x]
    yes.air_temperature_in_degrees_c = yes.air_temperature_in_degrees_c.astype('float64')
    min = yes.groupby(['dd'], sort=False)['air_temperature_in_degrees_c'].min()
    min.plot(kind='line', label=x)
plt.xlabel('Day')
plt.ylabel('Temperature in C\N{DEGREE SIGN}')
plt.title('Min Temperatures in October Per Year\n Station: ' + df.station_number[1].astype('str'))
plt.xticks(range(1, 32))
plt.grid(linestyle=':')
plt.legend()
# remove below comment to save figs
'''plt.savefig(
    'C:/Users/Josh/Documents/Uni/ProjectDesign/Processed/Test/min_' + df.station_number[1].astype('str') + '.png')'''
plt.show()

# Makes graphs showing Min, Max and Average Temperatures for each year in Ocrober
# Change min value in 'range' to dictate earliest year to get
for x in range(2014, 2019):
    temperature(x)
